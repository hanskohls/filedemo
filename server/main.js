import { Meteor } from 'meteor/meteor';

import url from 'url';
import http from 'http';
import base64 from 'base64-stream';

Meteor.startup(() => {
  // code to run on server at startup
});


Meteor.methods({
  "downloadUrl"(fileUrl){
    const host = url.parse(fileUrl).host;
    const path = url.parse(fileUrl).pathname;
    const options = {host, path, port: 80};
    let chunks = [];

    return new Promise(function(resolve, reject){
      console.log(options);
      http.get(options, function(res){
        
        res.on('data', (chunk)=>{
          //console.log(chunk.toString('base64'));
          chunks.push(chunk);
        });
        res.on('end', ()=>{
          console.log("returning");
          resolve(chunks);
        });
      });
    });
  },

  "environment"(key){
    return process.env[key];
  }
})