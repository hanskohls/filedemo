import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.download.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.url = new ReactiveVar("http://road.cc/sites/default/files/styles/main_width/public/images/News/malaysia-flag.jpg?itok=ZHKQV0q7");
  this.base64 = new ReactiveVar("");
});

Template.download.helpers({
  url() {
    return Template.instance().url.get();
  },
  name() {
    const url = Template.instance().url.get();
    return url.substring(url.lastIndexOf("/")+1);
  },

  b64(){
    const b64 = Template.instance().base64.get();
    if (!b64){
      return "";
    } else {
      return "data:image/jpg;base64," + b64;
    }
  },

  envValue(){
    return Meteor.call("environment", "MY_AWS_KEY" );
  }
});

Template.download.events({
  'input input'(e, t){
    t.url.set(e.currentTarget.value);
    console.log(t.url.get());
  },

  'click button'(e, t) {
    Meteor.call("downloadUrl", t.url.get(), (err, res)=>{
      if(res){
        console.log(res);
        const mimeType = 'image/jpeg';
        const blob = new Blob(res, {type: mimeType});
        const link = document.createElement('a');
        const url = window.URL.createObjectURL(blob);
        link.style = "display: none";
        link.href = url;
        link.download = "test.jpg";
        document.body.appendChild(link);
        link.click();
        window.URL.revokeObjectURL(url);
      }
    });
  },
});
